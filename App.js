/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * By Sherrod Jenkins
 */

import React, {Component} from 'react';
import {
  Platform,
  Text, 
  View, 
  ScrollView,
  TouchableHighlight, 
  ImageBackground
} from 'react-native';
import styles from './app/components/styles/index.style';
import menu from './app/components/menu/Menu';
import {createStackNavigator} from 'react-navigation';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import menuItem from './app/components/menu/menuItem';
import { ErrorBoundary } from './app/components/error/errorBoundary';
import addItem from './app/components/menu/addMenuItem';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    return <Provider store={store}>< RootStack /></Provider>;
  }
}


class HomeController extends Component {
  //View
  render(){
    return (
      <ErrorBoundary>
        <ScrollView style = {styles.view}>
          <React.Fragment>
            {this.collectionView()}
          </React.Fragment>
        </ScrollView>
      </ErrorBoundary>  
    );
  }

  //Naming the navigation route
  static navigationOptions = {
    title : 'Home',
  };


  //Functions
  _onPress = (image) => {
    this.props.navigation.navigate('Menu', {
        section: image.section,
    });
  }

  collectionView = () => {
    let imageSources = [{image: require('./app/assets/home/f&veggies.jpg'), section: 'lunch'}, {image: require('./app/assets/home/chineseFoods.jpeg'), section: 'dinner'}, {image: require('./app/assets/home/Food&Drink.jpg'), section: 'salad'}, {image: require('./app/assets/home/healthyFoods.jpg'), section: 'breakfast'}];

    var views = imageSources.map((images, key) => 
      <TouchableHighlight onPress={this._onPress.bind(this, images)} key={images.image}>
        <View>
          <ImageBackground
          style={styles.button}
          source={images.image}
          >
          <Text style={styles.text}>{images.section}</Text>
          </ImageBackground>
        </View>  
      </TouchableHighlight>
    )


    return views
  }
}
  

//Properties
const RootStack = createStackNavigator({ 

  Home: HomeController,
  Menu: menu,
  MenuItem: menuItem,
  AddItem: addItem,
},
{
  initialRouteName: 'Home',

  navigationOptions: {
    headerStyle: {
      backgroundColor: 'green',
    },
      headerTintColor: 'white',
    },
});
