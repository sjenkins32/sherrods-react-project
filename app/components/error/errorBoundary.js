/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * By Sherrod Jenkins
 */

import React, {Component} from 'react';
import {  
  Text, 
} from 'react-native';

export class ErrorBoundary extends Component {
  //Setting initial state
  constructor(props){
    super(props);
      this.state = {
      hasError: false
    }
  }

  //If any errors
  componentDidCatch(error, info){
    this.setState({
      hasError: true
    });
    console.log(error, info);
  }

  //Error View
  render(){
    if(this.state.hasError){
      return(
        <Text>OOPS Something Went Wrong!</Text>
      );
    }
    //Return children if no error
    return this.props.children
  }
}
