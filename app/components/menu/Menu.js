/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * By Sherrod Jenkins
 */
import React, {Component} from 'react';
import {  
  Text, 
  View, 
  ScrollView,
  TouchableHighlight, 
  Image,
  TouchableOpacity,
  Alert
 } from 'react-native';
import { connect } from 'react-redux';  
import styles from '../styles/menu/Menu.style';
import { fetchMenu } from '../../redux/actions/menuActions';
import { ErrorBoundary } from '../error/errorBoundary';
import PropTypes from 'prop-types';
import _ from 'lodash';

class Menu extends Component {
  constructor(props){
    super(props);
    this.state = {
      id: this.props.deletedItem,
      section: this.props.navigation.getParam('section', ''),
      title: this.props.navigation.getParam('title', '')
    }
  }

  //Lifecycle methods
  componentWillReceiveProps(nextProps){
    if(!_.isEmpty(nextProps.item)){
      this._addProp()
    } else if(!_.isEmpty(nextProps.deletedItem)){
      const indexOfDeletedItem = _.findIndex(this.props.menu, nextProps.deletedItem)
      delete this.props.menu[indexOfDeletedItem]
    } else if(!_.isEmpty(nextProps.edittedItem)){
      this._addProp()
    }
    console.log(this.props.edittedItem)
  }

  componentWillMount(){
    this.props.fetchMenu(this.state.section);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
      <TouchableOpacity onPress={() => navigation.navigate('AddItem', {
        section: navigation.getParam('section', 'nothing')
        }   
        )}>
        <Image
          style={styles.imageButton}
          source={require('../../assets/AddMenuItem/addIcon.jpg')}
        />
      </TouchableOpacity>
      ),
    };
  }
  
  
  //View 
  render(){
    return(
      <ErrorBoundary>
        <ScrollView style = {{flex: 1, flexDirection: 'column'}}>
          <React.Fragment>
            {this.collectionView()}
          </React.Fragment>
        </ScrollView>
      </ErrorBoundary>  
    );
  }

  //Functions
  _onPress = (items) => {
    this.props.navigation.push('MenuItem', {
      title: items.name,
      description: items.description,
      id: items.id,
      section: this.state.section
    })
  } 

  _addProp(){
    this.props.fetchMenu(this.state.section);
  }


  collectionView = () => {
    const views = this.props.menu.map((items) => 
      <TouchableHighlight onPress={this._onPress.bind(this, items)} key={items.id}>
        <View style={styles.view}>
          <Image
            style={styles.button}
            source={require('../../assets/home/f&veggies.jpg')}
          />
          <Text style={styles.title}> {items.name} </Text>
          <Text style={styles.description} numberOfLines={2}> {items.description}</Text>  
        </View>  
      </TouchableHighlight>
    )
    return views
  }
}

Menu.prototypes = {
  fetchMenu: PropTypes.func.isRequired,
  menu: PropTypes.array.isRequired,
  item: PropTypes.object,
  deletedItem: PropTypes.object
}

//Properties
//Map the state to the props
const mapStateToProps = state => ({
  menu: state.menu.items,
  item: state.menu.item,
  deletedItem: state.menu.deletedItem,
  edittedItem: state.menu.edittedItem
});

export default connect(mapStateToProps, { fetchMenu })(Menu);
