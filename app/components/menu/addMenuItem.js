/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * By Sherrod Jenkins
 */
import React, {Component} from 'react';
import {  
  Text, 
  View,
  TextInput,
  Button,
  Alert
} from 'react-native';
import styles from '../styles/menu/addItem.style';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { newItem } from '../../redux/actions/menuActions';

class AddMenuItem extends Component {
  //Constructor 
  constructor(props){
    super(props);
    this.state = {
      title: "",
      description: "",
    }
  }

  //Submit form button
  _submitButton = () => {
    const menuItem = {
      title: this.state.title,
      description: this.state.description,
      section: this.props.navigation.getParam('section', 'nothing')
    }
    if(this.state.title!="" && this.state.description!=""){
      this.props.newItem(menuItem);
      this.props.navigation.navigate('Menu');
    } else {
      Alert.alert("Fields cannot be empty")
    }
  }

  //View
  render(){
    return(
      <View style={styles.topView}>
        <Text style={styles.title}>Add New Item</Text>
				<View>
          <Text>Title</Text>
          <Text>{this.state.success}</Text>
          <TextInput
            style={styles.textInput}
            maxLength = {100}
            numberOfLines = {1}
            editable
            onChangeText={(title) => this.setState({title})}
            value={this.state.title}
          />
        </View>
				<View>
          <Text style={styles.descriptionButton}>Description</Text>
          <TextInput
            style={styles.textInput}
            maxLength = {100}
            numberOfLines = {4}
            editable
            onChangeText={(description) => this.setState({description})}
            value={this.state.description}
          />
        </View>
        <Button
          style={styles.submitButtonStyle}
          title="Button"
          onPress={this._submitButton}
        />
      </View>
    );
  }
}

AddMenuItem.prototypes = {
  newItem: PropTypes.func.isRequired
}

export default connect(null, { newItem })(AddMenuItem);
