/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * By Sherrod Jenkins
 */

import React, {Component} from 'react';
import { 
  Text, 
  View, 
  Image,
  TextInput,
  Button,
  Alert } from 'react-native';
import styles from '../styles/menuItem/menuItem.style';    
import { ErrorBoundary } from '../error/errorBoundary';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { editItem, deleteItem } from '../../redux/actions/menuActions';

class MenuItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      isEditting: false,
      description: this.props.navigation.getParam('description', 'Nothing'),
      title: this.props.navigation.getParam('title', 'Nothing'),
      id: this.props.navigation.getParam('id', 'nothing')
    }
  }

  _onSubmit = () => {
    const item = {
      title: this.state.title,
      description: this.state.description,
      section: this.props.navigation.getParam('section', 'nothing'),
      id: this.state.id
    }
    if(item.title!="" && item.description!=""){
      this.props.editItem(item, item.id);
      this.props.navigation.navigate('Menu');
    } else {
      Alert.alert("Fields cannot be empty")
    }
  }

  _editSelection = () => {
    this.setState({
      isEditting: true
    });
  }

  _deleteSelection = () => {
    const items = {
      title: this.state.title,
      id: this.state.id
    }
    this.props.deleteItem(items);
    this.props.navigation.navigate('Menu', {
      deletedId: items.id,
      title: items.title
    });
  }

  //Views
  render(){
    var titleView, descriptionView, submitButton;
    if(this.state.isEditting){
      titleView       = <TextInput
                          style={styles.textInput}
                          maxLength = {100}
                          numberOfLines = {1}
                          editable
                          onChangeText={(title) => this.setState({title})}
                          value={this.state.title}
                          placeholder="Enter Title"
                          />;
      descriptionView = <TextInput
                          style={styles.textInput}
                          maxLength = {100}
                          numberOfLines = {4}
                          editable
                          onChangeText={(description) => this.setState({description})}
                          value={this.state.description}
                          placeholder="Enter Description"
                          />;
      submitButton    = <Button
                          title="Save"
                          onPress={this._onSubmit}
                        />
    } else {
      titleView       = <Text style={styles.title}> {this.state.title} </Text>;
      descriptionView = <Text style={styles.description}> { this.state.description } </Text>;
    }
    return(
      <ErrorBoundary>
        <View style={styles.view}>
          <Button
            title="Edit"
            onPress={this._editSelection}
          />
          <Button
            title="Delete"
            onPress={this._deleteSelection}
          />
          <Image
            style={styles.image}
            source={require('../../assets/home/f&veggies.jpg')}
          />
          {titleView}
          {descriptionView}
          {submitButton}
        </View>  
      </ErrorBoundary>  
    );
  }
}

MenuItem.propTypes = {
  editItem: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  menu: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  menu: state.menu.items,
  item: state.menu.item
});

export default connect(mapStateToProps, { editItem, deleteItem })(MenuItem);
