import { StyleSheet } from 'react-native';


export default StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'stretch',
  },
  image: {
    aspectRatio: 2.1,
    height: 200,
  },
  title: {
    fontSize: 20,
  },
  description: {
    fontSize: 16
  },
  textInput: {
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    padding: 10,
  },
});
    