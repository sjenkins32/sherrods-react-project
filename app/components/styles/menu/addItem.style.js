import { 
  StyleSheet,
  Platform
} from 'react-native';

export default StyleSheet.create({
  textInput: {
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    padding: 10,
  },
  title: {
    textAlign: 'center',
    color: (Platform.OS === 'android' ? '#000402' : 'black'),
    fontSize: 25,
  },
  topView: {
    marginTop: 50,
  },
  submitButtonStyle: {
    color:'blue',
  },
  descriptionButton: {
    marginTop: 30,
  },
});
