import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: {
    width: 150,
    height: 100,
    alignSelf: 'baseline'
  },
  imageButton: {
    width: 25, 
    height: 25,
    marginRight: 20,
  },
  view: {
  	height: 115,
  	borderColor: 'black',
    borderWidth: 1,
    flex: 1, 
    flexDirection: 'column',
    alignItems: "flex-end",
  },
  title: {
    width: 200,
    marginTop: -75,
    fontSize: 15,
  },
  description: {
    width: 200,
    fontSize: 10,
  },
  spinnerTextStyle: {
    color: '#FFF'
  }
});
