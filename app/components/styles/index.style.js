import { StyleSheet } from 'react-native';
import reducers from '../../redux/reducers';

export default StyleSheet.create({
  button: {
    height: 200,
    borderColor: 'black',
    borderWidth: 1,
  },
  view: {
    flex: 1, 
    flexDirection: 'column',
    alignContent: 'stretch',
  },
  text: {
    fontSize: 50,
    color: '#ff0000',
    backgroundColor: '#ffffff',
  }
});
