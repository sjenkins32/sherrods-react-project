import { FETCH_MENU, NEW_ITEM, EDIT_ITEM, DELETE_ITEM } from './types';

export const fetchMenu = (section) => dispatch => {
  fetch('http://localhost:3000/mobile_api/menu?section=' + section)
  .then(res => res.json())
  .then(menu => dispatch({
    type: FETCH_MENU,
    payload: menu
  }))
}

export const newItem = (menuItem) => dispatch => {
  fetch('http://localhost:3000/mobile_api/menu', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(menuItem)
  })
  .then(res => res.json())
  .then(item => dispatch({
    type: NEW_ITEM,
    payload: item
  }))
}

export const editItem = (item, id) => dispatch => {
  fetch('http://localhost:3000/mobile_api/menu/' + id, {
    method: 'PATCH',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(item)
  })
  .then(res => res.json())
  .then(edittedItem => dispatch({
    type: EDIT_ITEM,
    payload: edittedItem
  }))
}

export const deleteItem = (items) => dispatch => {
  fetch('http://localhost:3000/mobile_api/menu/' + items.id, {
    method: 'DELETE',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(items)
  })
  .then(res => res.json())
  .then(deletedItem => dispatch({
    type: DELETE_ITEM,
    payload: deletedItem
  }))
}
