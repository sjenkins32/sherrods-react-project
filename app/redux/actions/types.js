export const FETCH_MENU = 'fetch_menu';
export const NEW_ITEM = 'new_item';
export const EDIT_ITEM = 'edit_item';
export const DELETE_ITEM = 'delete_item';

