import { FETCH_MENU, NEW_ITEM, EDIT_ITEM, DELETE_ITEM } from '../actions/types';

const initialState = {
  items: [],
  item: {},
  deletedItem: {},
  edittedItem: {}
}

export default function (state = initialState, action) {
  switch(action.type) {
    case FETCH_MENU:
      return {
        ...state, 
        items: action.payload
      }
    case NEW_ITEM:
    return {
      ...state, 
      item: action.payload
    } 
    case EDIT_ITEM:
    return {
      ...state, 
      edittedItem: action.payload
    } 
    case DELETE_ITEM:
    return {
      ...state, 
      deletedItem: action.payload
    } 
    default: 
      return state  
  }
}